<?php
/* Smarty version 3.1.33, created on 2020-02-10 20:00:10
  from '/Applications/MAMP/htdocs/lalola/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e41a83a4892f0_64609086',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '171cae814d0f75588617e7b13648e5e9ced2139a' => 
    array (
      0 => '/Applications/MAMP/htdocs/lalola/themes/classic/templates/index.tpl',
      1 => 1581360859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e41a83a4892f0_64609086 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16888359435e41a83a484445_43724003', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_1053582015e41a83a484ea1_50879793 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_2834642685e41a83a4863d7_15137996 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_276343045e41a83a485b86_32100488 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2834642685e41a83a4863d7_15137996', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_16888359435e41a83a484445_43724003 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_16888359435e41a83a484445_43724003',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_1053582015e41a83a484ea1_50879793',
  ),
  'page_content' => 
  array (
    0 => 'Block_276343045e41a83a485b86_32100488',
  ),
  'hook_home' => 
  array (
    0 => 'Block_2834642685e41a83a4863d7_15137996',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1053582015e41a83a484ea1_50879793', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_276343045e41a83a485b86_32100488', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
